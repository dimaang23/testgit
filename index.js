let transferResults = [],
    positionTransferredFigure = [],
    arrangementFigures = { pentagon: 0, hexagon: 0, trapezium: 0, triangle: 0 };

// common commands

function settingStandardStylesForSquare(element) {
    element.style.border = "1px solid #d0d0d0";
    element.style.background = "#f4f4f4";
}

function unique(arr) {
    let result = [];

    for (let str of arr) {
        if (!result.includes(str) && str !== 0) {
            result.push(str);
        }
    }

    return result;
}

// other commands

function dragStart(event) {
    localStorage.setItem("id", event.target.id);
    localStorage.setItem("parentElementId", event.target.parentElement.id);
    localStorage.setItem("squareId", event.target.parentElement.id);
    localStorage.setItem("positionRow", event.target.dataset.position);

    if (event.target.parentElement.className === "figure") {
        let element =
            document.getElementsByClassName("figure")[
                event.target.dataset.position
            ];
        element.style.opacity = "0.5";
    }
}

function allowDrop(event) {
    event.preventDefault();
}

function dragLeave(event) {
    if (event.target.className === "figure") {
        let element =
            document.getElementsByClassName("figure")[
                localStorage.getItem("positionRow")
            ];

        element.style.opacity = "0.5";
    }

    if (event.target.className === "square") {
        settingStandardStylesForSquare(event.target);
    }
}

function dragEnter(event) {
    if (event.target.className === "square") {
        event.target.style.border = "1px solid #369CB7";
        event.target.style.background = "white";
    }
}

// not over the square

function dropNotOverSquare(figureId) {
    let pId = localStorage.getItem("parentElementId");
    let sId = localStorage.getItem("squareId");
    let clearedArea = document.getElementById(pId);

    document.getElementsByClassName("figure")[
        localStorage.getItem("positionRow")
    ].innerHTML = "";

    clearedArea.innerHTML = "";
    settingStandardStylesForSquare(clearedArea);

    document.getElementsByClassName("figure")[
        localStorage.getItem("positionRow")
    ].style.opacity = "1";

    let newObjectBeingCreated = document.createElement("img");
    newObjectBeingCreated.src = "./pictures/" + figureId + ".svg";
    newObjectBeingCreated.id = figureId;
    newObjectBeingCreated.alt = figureId;

    newObjectBeingCreated.setAttribute(
        "data-state",
        localStorage.getItem("numberSidesFigure")
    );
    newObjectBeingCreated.setAttribute(
        "data-position",
        localStorage.getItem("positionFigure")
    );

    document.getElementById(
        localStorage.getItem("parentElementId")
    ).style.cursor = "default";

    let newPlaceSelectedFigure =
        document.getElementsByClassName("figure")[
            localStorage.getItem("positionRow")
        ];
    newPlaceSelectedFigure.appendChild(newObjectBeingCreated);

    document.getElementById(sId).innerHTML = "";
    settingStandardStylesForSquare(document.getElementById(sId));
}

// over the square

function dropOverSquare(event, figureId) {
    clearAllBlocksExceptRequired();

    let selectedFigure = document.getElementById(figureId);
    let numberSidesFigure = selectedFigure.dataset.state;
    let positionFigure = selectedFigure.dataset.position;

    localStorage.setItem("numberSidesFigure", numberSidesFigure);
    localStorage.setItem("positionFigure", positionFigure);

    let selectedSquare = document.getElementById(event.target.id);
    let numberSidesSquare = selectedSquare.dataset.state;

    if (numberSidesSquare !== numberSidesFigure) {
        transferResults.push("неверно");
    } else {
        transferResults.push("верно");
    }

    creatingPastingCopyShapePlaceOriginal(
        figureId,
        numberSidesFigure,
        positionFigure
    );

    event.target.appendChild(document.getElementById(figureId));
    event.target.style.background = "white";
    event.target.style.border = "none";
    event.target.style.cursor = "grab";
}

function clearAllBlocksExceptRequired() {
    let squareElements = document.querySelectorAll(".square");

    for (let square of squareElements) {
        for (let index in arrangementFigures) {
            if (square.id !== arrangementFigures[index]) {
                positionTransferredFigure.push(arrangementFigures[index]);
            }
        }
    }

    positionTransferredFigure = unique(positionTransferredFigure);
    positionTransferredFigure = [positionTransferredFigure.pop()];

    let arraySize = 0;
    let occupiedSquares = [];
    let whichSquaresOccupied = [];

    for (let index in arrangementFigures) {
        if (arrangementFigures[index] !== 0) {
            arraySize++;
            occupiedSquares.push(arrangementFigures[index]);
        }
    }

    if (arraySize > positionTransferredFigure.length) {
        whichSquaresOccupied =
            positionTransferredFigure.concat(occupiedSquares);
        whichSquaresOccupied = unique(whichSquaresOccupied);
    }

    let occupiedSquaresWithClearingOthers = [];

    if (whichSquaresOccupied.length === 0) {
        for (let square of squareElements) {
            for (let index of positionTransferredFigure) {
                if (square.id !== index) {
                    let rid = document.getElementById(square.id);
                    rid.innerHTML = "";
                    settingStandardStylesForSquare(rid);
                }
            }
        }
    } else {
        for (let square of squareElements) {
            for (let index of whichSquaresOccupied) {
                if (square.id === index) {
                    occupiedSquaresWithClearingOthers.push(square.id);
                }
            }
        }
    }

    occupiedSquaresWithClearingOthers = unique(
        occupiedSquaresWithClearingOthers
    );

    if (occupiedSquaresWithClearingOthers) {
        if (occupiedSquaresWithClearingOthers.length === 4) {
            let freeSquares = document.querySelectorAll(
                ".square:not([id='" +
                    occupiedSquaresWithClearingOthers[0] +
                    "'], [id='" +
                    occupiedSquaresWithClearingOthers[1] +
                    "'], [id='" +
                    occupiedSquaresWithClearingOthers[2] +
                    "'], [id='" +
                    occupiedSquaresWithClearingOthers[3] +
                    "'])"
            );

            clearSquare(freeSquares);
        } else if (occupiedSquaresWithClearingOthers.length === 3) {
            let freeSquares = document.querySelectorAll(
                ".square:not([id='" +
                    occupiedSquaresWithClearingOthers[0] +
                    "'], [id='" +
                    occupiedSquaresWithClearingOthers[1] +
                    "'], [id='" +
                    occupiedSquaresWithClearingOthers[2] +
                    "'])"
            );

            clearSquare(freeSquares);
        } else if (occupiedSquaresWithClearingOthers.length === 2) {
            let freeSquares = document.querySelectorAll(
                ".square:not([id='" +
                    occupiedSquaresWithClearingOthers[0] +
                    "'], [id='" +
                    occupiedSquaresWithClearingOthers[1] +
                    "'])"
            );

            clearSquare(freeSquares);
        } else if (occupiedSquaresWithClearingOthers.length === 1) {
            let freeSquares = document.querySelectorAll(
                ".square:not([id='" +
                    occupiedSquaresWithClearingOthers[0] +
                    "'])"
            );

            clearSquare(freeSquares);
        }
    }

    function clearSquare(freeSquares) {
        for (let selectedSquare of freeSquares) {
            selectedSquare.innerHTML = "";
            settingStandardStylesForSquare(selectedSquare);
        }
    }
}

function creatingPastingCopyShapePlaceOriginal(
    figureId,
    numberSidesFigure,
    positionFigure
) {
    let objectBeingCreated = document.createElement("img");
    objectBeingCreated.src = "./pictures/" + figureId + ".svg";
    objectBeingCreated.id = figureId;
    objectBeingCreated.alt = figureId;
    objectBeingCreated.style.opacity = "1";
    objectBeingCreated.style.cursor = "default";
    objectBeingCreated.setAttribute("data-state", numberSidesFigure);
    objectBeingCreated.setAttribute("data-position", positionFigure);

    let placeSelectedFigure =
        document.getElementsByClassName("figure")[
            localStorage.getItem("positionFigure")
        ];

    placeSelectedFigure.appendChild(objectBeingCreated);
}

// the end of logical thought

function drop(event) {
    localStorage.setItem("parentElementId", event.target.id);

    switch (localStorage.getItem("id")) {
        case "hexagon":
            arrangementFigures.hexagon =
                localStorage.getItem("parentElementId");
            break;
        case "pentagon":
            arrangementFigures.pentagon =
                localStorage.getItem("parentElementId");
            break;
        case "trapezium":
            arrangementFigures.trapezium =
                localStorage.getItem("parentElementId");
            break;
        case "triangle":
            arrangementFigures.triangle =
                localStorage.getItem("parentElementId");
            break;
    }

    let figureId = localStorage.getItem("id");

    if (event.target.className !== "square") {
        dropNotOverSquare(figureId);
    } else {
        dropOverSquare(event, figureId);
    }
}

function clickHandler() {
    alert("Итог - " + transferResults);
}
